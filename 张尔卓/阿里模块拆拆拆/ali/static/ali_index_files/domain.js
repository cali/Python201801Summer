jQuery(function($) {

	// hack 在前端判断隐藏 首页 并修改研究领域分类的class
	(function indexHack() {
		
		// 如果是首页
		if (document.location.pathname == '/' || document.location.pathname == '/blog.html' || document.location.pathname == '/ali/aliresearch/webroot/') {
			$(".onresearch")[0].className = "on";
			$(".navList").find(".hover").hide();
			
			// 2015-1-22 首页额外覆盖一层class
			$("body").addClass("blog-body");
			
			// 2015-2-2 隐藏首页评论数
			var list = $(".j-hotspot-list").find(".j-hotsBtn").children();
			list.each(function(index){
				if (index % 3 != 0) {
					$(list[index]).hide();
				}
			});
		}
	})();
	
	 domainIndexPost(1,0);
	 getDynamic();
	 bindLogout();
	 setTimeout("getDocuments(1)",1000);
	
	//首页推荐圈子更多
	$("#ali_owen_circle_btn").click(function(){
		if($(this).hasClass('btnOpen')) {
			$(this).attr('class', 'btnColse');
			$("#ali_owen_circle li").each(function(){
				if($(this).css('display') == 'none') {
					$(this).attr('default',1);
					$(this).show();
				}
			});
		}
		else {
			$(this).attr('class', 'btnOpen');
			$("#ali_owen_circle li").each(function(){
				if($(this).attr('default') == "1") {
					$(this).hide();
				}
			});
		}
	});
	//查找
	$("#global_search_btn").click(function(){
		var v = $("input[name='keywords']").val();
		if(!v) alert('请输入关键词！');
		$("#global_search").submit();
	});

	//帖子点击
	$(".post_list_btn").click(function(){
		var v = parseInt($(this).attr('is_top'));
		$(".post_list_btn").removeClass('on');
		$(this).addClass('on');
		domainIndexPost(1, v);
	});

	//领域首页资讯读取
	$("#document_menu li").click(function(){
		var tag_id = $(this).attr('data');
		if(!tag_id) {
			alert('缺少tag_id!');
			return false;
		}
		$("#document_menu li").removeClass('on');
		$(this).addClass('on');
		getDocuments(tag_id);
	});

	//event
	$('.J_logined').mouseenter(function(){
		$('.J_logined').addClass('hover');
	});
	$('.J_logined').mouseleave(function(){
		$('.J_logined').removeClass('hover');
	});
	$('.J_pcNav li').mouseenter(function(){
		$(this).addClass('hover');
	});
	$('.J_pcNav li').mouseleave(function(){
		$(this).removeClass('hover');
	});
	$('.J_qMark').mouseenter(function(){
		$('.qMarkTip',this).show();
	});
	$('.J_qMark').mouseleave(function(){
		$('.qMarkTip',this).hide();
	});
	$('.J_mMenu').click(function(){
		if ($('.m-navMenu').hasClass('db')){
			$('.m-navMenu').removeClass('db');
		}else{
			$('.m-navMenu').addClass('db');
		}
	});

	$('.m-menuNavType').click(function(){
		var _isShow = $('.area_list').hasClass('listShow');
		if(_isShow){
			$('.area_list').css('height','0');
			$('.area_list').removeClass('listShow');
		}else if(!_isShow){
			$('.area_list').css('height','auto');
			$('.area_list').addClass('listShow');
		}
	})
	
	//页面空白点击
	$(document).click(function(e){
		$('.eweiImg').hide();
	});
	//弹出
	$('.ewei').click(function(e){		
		oEvent= e||event;
		oEvent.cancelBubble=true;
		$(".eweiImg").css({'top':oEvent.pageY-172,'left':oEvent.pageX-9});
		if($(this).css('display') != 'block')
			$('.eweiImg').show();
		return false;
	});

	/**
	 * 绑定登出事件
	 */
	function bindLogout() {
		$('[event-node=logout]').click(function () {
			$.get('/Usercenter/System/logout.html', function (msg) {
				alert(msg.message);
				location.href = msg.url;
			}, 'json')
		});
	}

	initAspiCharts(aspiOption);

	$(".index-header-aspi-tab").find('li').on("mouseover", function() {
		$(this).addClass('active').siblings().removeClass('active');

		if ($(this).html() == 'aSPI') {
			initAspiCharts(aspiOption);
		} else {
			initAspiCharts(aspiCoreOption);
		}

	})

	function initAspiCharts(option) {
		// 设置折线图
		var aspiChart = echarts.init(document.getElementById('aspiCharts'));
		//var option = option;
		aspiChart.setOption(option);

		// 设置表格
		var html = [];
		html.push('<table><tbody>');

		html.push('		<tr>');
		html.push('			<th class="text">月份</th>');
		for (var i = 0; i < 3; i++) {
			var month = option.xAxis[0].data.slice(-3 + i)[0];
			html.push('<th>' + month.slice(0, 4) + '.' + month.slice(-2) + '</th>')
		}
		html.push('		</tr>');

		html.push('		<tr>');
		html.push('			<th class="text">月环比</th>');
		for (var i = 0; i < 3; i++) {
			var v = option.series[0].data.slice(-3 + i)[0];
			html.push('<th class="' + (v > 0 ? 'up' : 'down') + '">' + v  + '%</th>')
		}
		html.push('		</tr>');

		html.push('		<tr>');
		html.push('			<th class="text">年同比</th>');
		for (var i = 0; i < 3; i++) {
			var v = option.series[1].data.slice(-3 + i)[0];
			html.push('<th class="' + (v > 0 ? 'up' : 'down') + '">' + v  + '%</th>')
		}
		html.push('		</tr>');

		html.push('</tbody></table>');

		document.getElementById('aspiMoreData').innerHTML = html.join('');
	}
});

function getDocuments(tag_id,cat_id,page) {
	//领域首页滚动【他们正在】
	var obj = $('.userDoingList');
	var url = '/circle/api/getDocuments.html';
	var cat_id = cat_id?cat_id:obj.attr('cat_id');
	$.ajax({
		url: url,
		type: 'POST',
		data:{id:cat_id,tag_id:tag_id,page:page},
		dataType: 'json',
		timeout: 10000,
		beforeSend:function(){	
		},
		error: function(){
			//alert('Error loading PHP document');
		},
		success: function(data){
			if(data.info.data == '') {
				$("#documents_list").html('<li>暂无信息！</li>');
				$('#document_list_page_mobile').html('');
				$("#document_list_page").html('');
			}
			else {
				$("#documents_list").html(data.info.data);
				$("#document_total").html(data.info.totalCount);
				$("#document_list_page").html(data.info.pageHtml);
				if(data.info.page != '') {
					var page_mobile = '<a ';
					if(page<=1)
						page_mobile += 'class="off"';
					page_mobile += 'href="javascript:;" onclick="getDocuments('+tag_id+','+cat_id+','+data.info.prevPage+')">上一页</a>';
					page_mobile += '<a class="next off" href="javascript:;" onclick="getDocuments('+tag_id+','+cat_id+','+data.info.nextPage+')">下一页</a>';
					$('#document_list_page_mobile').html(page_mobile);
				}
			}
		}
	});
}

function getDynamic() {
	//领域首页滚动【他们正在】
	var obj = $('.userDoingList');
	var url = '/circle/api/getDynamicOfDomain.html';
	var cat_id = obj.attr('cat_id');
	$.ajax({
		url: url,
		type: 'POST',
		data:{id:cat_id},
		dataType: 'json',
		timeout: 10000,
		beforeSend:function(){	
		},
		error: function(){
			//alert('Error loading PHP document');
		},
		success: function(data){
			if(data.info == '') {
				obj.find('span').text('暂无信息！');
			}
			else {
				obj.find('span').remove();
				obj.find('ul').append(data.info);
				gundong();
			}
		}
	});
}

//doing
function gundong() {
	var dSpeed = 2000;
	var dulh = $('.J_doingUl').height();
	var dlih = $('.J_doingUl li:first').outerHeight();
	var dliL = $('.J_doingUl li').length;
	var doing = $('.J_doingBox').html();
	$('.J_doingBox').append(doing);
	function Marquee(){
		var dBoxTop = parseInt($('.J_doingBox').css('top'));
		dBoxTop = dBoxTop - dlih;
		$('.J_doingBox').animate({'top':dBoxTop+'px'},function(){
			if(dBoxTop == (-dulh)){
				dBoxTop = 0;
				$('.J_doingBox').css({'top':dBoxTop+'px'});
			}
		});
	}
	if(dulh==0||dliL<=3){
	}else{
		var mar = setInterval (Marquee,dSpeed);
	};
	$('.userDoingList').mouseenter(function(){
		clearInterval(mar);
	});
	$('.userDoingList').mouseleave(function(){ 
		mar = setInterval(Marquee,dSpeed);
	});
}

//领域首页帖子|精华
function domainIndexPost(page,is_top) {
	
	var obj = $('.userDoingList');
	var url = '/circle/api/getTopPostsOfDomain.html';
	var cat_id = obj.attr('cat_id');
	$.ajax({
		url: url,
		type: 'POST',
		data:{id:cat_id,page:page,is_top:is_top},
		dataType: 'json',
		timeout: 10000,
		beforeSend:function(){	
		},
		error: function(){
			//alert('Error loading PHP document');
		},
		success: function(data){
			if(data.info.list == '') {
				$('#post_list').html('<br/>暂无信息！');
				$('#post_list_page_mobile').html('');
				$('#post_list_page').html('');
			}
			else {
				$('#post_list').html(data.info.list);
				$('#post_list_page').html(data.info.page);
				if(data.info.page != '') {
					var page_mobile = '<a ';
					if(page<=1)
						page_mobile += 'class="off"';
					page_mobile += 'href="javascript:;" onclick="domainIndexPost('+data.info.prevPage+',0)">上一页</a>';
					page_mobile += '<a class="next off" href="javascript:;" onclick="domainIndexPost('+data.info.nextPage+',0)">下一页</a>';
					$('#post_list_page_mobile').html(page_mobile);
				}
			}
		}
	});
}

