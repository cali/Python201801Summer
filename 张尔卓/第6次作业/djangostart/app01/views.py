from django.shortcuts import render,HttpResponse

# Create your views here.
#视图函数：处理http请求
#request，是浏览器发送过来的数据
def demo01(request):
    print(request)
    html = "<h1>  I AM DEMO </h1>"
    print(html)
    return HttpResponse(html)


from .models import UserInfo

def demo02(request):
    #html模板名——————》字符串
    # 如果你用get方法提交，用request.get获取数据
    print(request.GET)
    # 如果key不存在，会有异常
    #email = request.GET("email")
    # 如果key不存在会，会返回一个默认值
    email = request.GET.get('email','103037788@qq.com')
    password = request.GET.get('password', 'abc')

    #跟数据库交互
    #插入记录

    UserInfo.objects.create(email=email, password=password)
    # 查询数据库中的所有信息
    userlist = UserInfo.objects.all()
    print(userlist)

    print(f"email:{email},password:{password}")
    # render的第三个参数，是字典
    # html_email,heml_password 实在html上用的
    #email/password 是变量
    return render(request,'hello.html',{"html_email":email,"html_password":password,'user_list':userlist})



def demo03(request):
    #html模板名——————》字符串
    return render(request,'zez_denglu.html')

def demo04(request):
    #html模板名——————》字符串

    return render(request,'zez_zhuce.html')

from .models import UserInfo1

def demo05(request):
    #html模板名——————》字符串
    # 如果你用get方法提交，用request.get获取数据
    # print(request.GET)
    # 如果key不存在，会有异常
    #email = request.GET("email")
    # 如果key不存在会，会返回一个默认值
    name = request.GET.get('name','zez')
    password = request.GET.get('password', 'abc')
    phonenumber =request.GET.get('phonenumber','18229997272')
    if len(name)<5 or len(name)>15:
        return render(request, 'register.html', {"html_error": name})

    elif  len(phonenumber)!=11:
        return render(request, 'register.html', {"html_number": phonenumber})

    elif len(password)<6:
        return render(request, 'register.html', {"html_word": password})
    #  跟数据库交互
    # 插入记录
    else:
        UserInfo1.objects.create(name=name, password=password, phonenumber=phonenumber)

        # # 查询数据库中的所有信息
        userlist = UserInfo1.objects.all()
        print(userlist)
        #
        print(f"name:{name},password:{password},phonenumber;{phonenumber}")
        # # render的第三个参数，是字典
        # html_email,heml_password 实在html上用的
        # email/password 是变量
        return render(request, 'register.html',
                      {"html_name": name, "html_password": password, 'html_phonenumber': phonenumber})
        # return render(request, 'register.html')