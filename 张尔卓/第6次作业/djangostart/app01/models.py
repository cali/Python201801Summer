from django.db import models

# Create your models here.

#一个类对应一个表
class UserInfo(models.Model):
    #多出id字段： 如果一个表里面没有主键，那么，它会自动生成一个名为id的主键
    email = models.EmailField()
    password =models.CharField(max_length=64)


class UserInfo1(models.Model):
    # 多出id字段： 如果一个表里面没有主键，那么，它会自动生成一个名为id的主键
    name = models.EmailField()
    password = models.CharField(max_length=64)
    phonenumber = models.CharField(max_length=11)
