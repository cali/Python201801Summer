"""djangostart URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
#导入模块，取名微app01_views
from app01 import views as app01_views

urlpatterns = [
    # admin 后台路由
    url(r'^admin/', admin.site.urls),
    # admin 自定义路由
    url(r'^demo01/$',app01_views.demo01),
    #
    url(r'demo02/$',app01_views.demo02),
    url(r'login/$',app01_views.demo03),
    url(r'register/$',app01_views.demo04),
    url(r'demo05/$',app01_views.demo05),
]
