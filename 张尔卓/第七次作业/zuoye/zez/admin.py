from django.contrib import admin

# Register your models here.
from .models import Student,Student1,Course
class StudentAdmin(admin.ModelAdmin):

    list_display = ('username', 'sex', 'city', 'score')
    list_editable = ('city',)
    list_display_links = ('username',)
    list_filter = ('sex','score')
    search_fields = ('username', 'score')

admin.site.register(Student, StudentAdmin)
admin.site.register(Student1)
admin.site.register( Course)

