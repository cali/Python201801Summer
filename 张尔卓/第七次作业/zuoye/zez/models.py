from django.db import models


from django.core.exceptions import ValidationError
def check_score(value):
    if value < 0 or value > 666:
        raise ValidationError("成绩必须在0-666之间")


class Student(models.Model):
    student_id = models.AutoField(primary_key=True)
    username=models.CharField(verbose_name="真实姓名", max_length=10, db_index=True, help_text="学生真实姓名")

    dexc = models.TextField(verbose_name="学生介绍", blank=True, null=True, db_column="description",
                         help_text="<h1>填写学生特长信息</h1>")

    SEX_CHOICE=(
            (0,"男生"),
            (1,"女生"),
            (None,"请选择性别"),
        )
    sex = models.IntegerField(choices=SEX_CHOICE)

    CITY_CHOICE = (
        ('湖南', (
            ('湖南01', '长沙'),
            ('湖南02', '株洲')
        )),
        ('四川', (
            ('四川01', '成都'),
            ('四川02', '稻城')
        )),
        (None, "请选择")
    )
    city = models.CharField(max_length=4, choices=CITY_CHOICE)
    admission_data = models.DateField(default='2018-01-24')
    score =models.FloatField(validators=[check_score,])
    schooling=models.DecimalField(max_digits=7,decimal_places=2,default=10000)
    email = models.EmailField()
    last_login_ip= models.GenericIPAddressField()
    is_login =models.BooleanField()
    is_login2 = models.NullBooleanField()
    avator = models.ImageField(upload_to='avator/',default='avator/default.png')
    is_editedable = models.IntegerField( blank=True,null= True, editable=False)
    is_unique = models.CharField(max_length=10, unique=True)


    def save(self,*args,**kwargs):

        if self.sex == 0:
            self.username = self.username + "帅哥"
        elif self.sex == 1:
            self.username = self.username + "美女"
        super().save(*args, **kwargs)

    def __str__(self):
         return self.username+str(self.score)


    def myname(self):
             print(self.username)

    class Meta:


                verbose_name_plural = '学生表01'
                get_latest_by = "english_score"
                ordering = ['username', '-student_id',]


from django.contrib.auth.models import User


class Course(models.Model):

    course_name = models.CharField(verbose_name='课程名', max_length=10)
    teacher = models.ForeignKey(User, verbose_name="任课老师", related_name='course_teacher_user')
    monitor = models.OneToOneField(Student, verbose_name="课代表", related_name="course_monitor_user")

    def __str__(self):
        return self.course_name

    class Meta:

            verbose_name_plural = '课程表'

class Student1(models.Model):
    student_name = models.CharField(verbose_name='学生姓名', max_length=10)

    cousrses = models.ManyToManyField(Course, verbose_name="我的课程")

    def __str__(self):
        return self.student_name


    class Meta:
        verbose_name_plural= '学生表02'