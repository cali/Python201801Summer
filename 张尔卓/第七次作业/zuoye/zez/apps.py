from django.apps import AppConfig


class ZezConfig(AppConfig):
    name = 'zez'
