# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-30 12:30
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import zez.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Course',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('course_name', models.CharField(max_length=10, verbose_name='课程名')),
                ('monitor', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='course_monitor_user', to=settings.AUTH_USER_MODEL, verbose_name='课代表')),
                ('teacher', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='course_teacher_user', to=settings.AUTH_USER_MODEL, verbose_name='任课老师')),
            ],
            options={
                'verbose_name_plural': '课程表',
            },
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('student_id', models.AutoField(primary_key=True, serialize=False)),
                ('username', models.CharField(db_index=True, help_text='学生真实姓名', max_length=10, verbose_name='真实姓名')),
                ('dexc', models.TextField(blank=True, db_column='description', help_text='<h1>填写学生特长信息</h1>', null=True, verbose_name='学生介绍')),
                ('sex', models.IntegerField(choices=[(0, '男生'), (1, '女生'), (None, '请选择性别')])),
                ('city', models.CharField(choices=[('湖南', (('湖南01', '长沙'), ('湖南02', '株洲'))), ('四川', (('四川01', '成都'), ('四川02', '稻城'))), (None, '请选择')], max_length=4)),
                ('admission_data', models.DateField(default='2018-01-24')),
                ('score', models.FloatField(validators=[zez.models.check_score])),
                ('schooling', models.DecimalField(decimal_places=2, default=10000, max_digits=7)),
                ('email', models.EmailField(max_length=254)),
                ('last_login_ip', models.GenericIPAddressField()),
                ('is_login', models.BooleanField()),
                ('is_login2', models.NullBooleanField()),
                ('avator', models.ImageField(default='avator/default.png', upload_to='avator/')),
                ('is_editedable', models.IntegerField(blank=True, editable=False, null=True)),
                ('is_unique', models.CharField(max_length=10, unique=True)),
            ],
            options={
                'verbose_name_plural': '学生表01',
                'ordering': ['username', '-student_id'],
                'get_latest_by': 'english_score',
            },
        ),
        migrations.CreateModel(
            name='Student1',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('student_name', models.CharField(max_length=10, verbose_name='学生姓名')),
                ('cousrses', models.ManyToManyField(to='zez.Course', verbose_name='我的课程')),
            ],
            options={
                'verbose_name_plural': '学生表02',
            },
        ),
    ]
