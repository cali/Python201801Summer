from django.shortcuts import render,HttpResponse

# Create your views here.
#视图函数：处理http请求
#request，是浏览器发送过来的数据
def demo01(request):
    print(request)
    html = "<h1>  I AM DEMO </h1>"
    print(html)
    return HttpResponse(html)

def demo02(request):
    #html模板名——————》字符串
    return render(request,'hello.html')

def demo03(request):
    #html模板名——————》字符串
    return render(request,'zez_denglu.html')

def demo04(request):
    #html模板名——————》字符串
    return render(request,'zez_zhuce.html')