from django.db import models
from django.core.exceptions import ValidationError# Create your models here.
#创建学生表
class eton_student(models.Model):

    SEX=(
        ("male","男生"),
        ("female","女生"),
        ("none","请选择性别")
    )
    def grade_judge(value):
        if value > 100 or value < 0:
            raise ValidationError("成绩必须在0-100之间")
    def phonenumber_judge(value):
        if len(value) !=11:
            raise ValidationError("请输入11位的手机号码")

    username=models.CharField(max_length=6,verbose_name="姓名")

    sex=models.CharField(max_length=6,verbose_name="性别",choices=SEX)

    birth = models.DateField("出生")

    grade=models.DecimalField("成绩",max_digits=4,decimal_places=1,validators=[grade_judge])


    email=models.CharField(max_length=12,null=True)

    charge=models.DecimalField("学费",max_digits=8,decimal_places=2,editable=False,default=15000.00)

    feature=models.TextField("特长",help_text="个人特长与爱好")

    class Meta:
        verbose_name = '学生信息表'
        verbose_name_plural = verbose_name
        db_table = "eton_students"

    def __str__(self):
        return self.username

    def save(self, *args, **kwargs):
        print("student保存前")
        length=len(self.username)
        if self.sex == "male":
            if self.username[length-1]!="哥":
                self.username = self.username + "帅哥"
        elif self.sex == "female":
            if self.username[length - 1] != "女":
                self.username = self.username + "美女"
        super().save(*args, **kwargs)
        print("student保存后")


#创建老师表
class eton_teacher(models.Model):
    SEX = (
        ('male', "男生"),
        ('female', "女生"),
        ('none',"请输入性别")
    )

    username = models.CharField(max_length=6, verbose_name="姓名")

    sex = models.CharField(max_length=6, verbose_name="性别", choices=SEX)

    age=models.IntegerField()

    desc=models.TextField(db_column="descption",null=True)

    class Meta:

        verbose_name = '老师信息表'

        verbose_name_plural = verbose_name

        db_table = "eton_teacher"

        ordering = ['age']

    def __str__(self):
        return self.username

class subject(models.Model):
    subject=models.CharField("科目",max_length=10,primary_key=True)

    teacher=models.ForeignKey(eton_teacher,verbose_name="任课老师")

    student=models.OneToOneField(eton_student ,verbose_name="课代表")
    class Meta:

        verbose_name = '科目信息表'

        verbose_name_plural = verbose_name

        db_table = "subject"


    def __str__(self):
        return self.subject
