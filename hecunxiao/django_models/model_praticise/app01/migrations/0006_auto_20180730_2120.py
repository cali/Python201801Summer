# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-30 13:20
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app01', '0005_auto_20180730_2114'),
    ]

    operations = [
        migrations.AlterField(
            model_name='eton_teacher',
            name='sex',
            field=models.CharField(choices=[('male', '男生'), ('female', '女生'), ('none', '请输入性别')], max_length=6, verbose_name='性别'),
        ),
    ]
