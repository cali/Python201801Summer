# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-30 13:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app01', '0004_auto_20180730_2105'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='eton_student',
            options={'verbose_name': '学生信息表', 'verbose_name_plural': '学生信息表'},
        ),
        migrations.AlterModelOptions(
            name='eton_teacher',
            options={'ordering': ['age'], 'verbose_name': '老师信息表', 'verbose_name_plural': '老师信息表'},
        ),
        migrations.AlterField(
            model_name='eton_teacher',
            name='sex',
            field=models.CharField(choices=[('male', '男生'), ('female', '女生'), ('none', '请选择性别')], max_length=6, verbose_name='性别'),
        ),
    ]
