from django.contrib import admin

# Register your models here.
from .models import eton_student,eton_teacher,subject

class student(admin.ModelAdmin):
    list_display =("username","sex","grade","birth","charge","email")
    list_editable =("sex","grade")
    list_display_links =("username",)
    list_filter = ("birth",)
    search_fields = ("username","grade")
    exclude = ["charge"]

admin.site.register(eton_student,student)
admin.site.register(eton_teacher)
admin.site.register(subject)