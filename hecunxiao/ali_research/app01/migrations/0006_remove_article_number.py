# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-31 15:26
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app01', '0005_auto_20180731_2322'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='article',
            name='number',
        ),
    ]
