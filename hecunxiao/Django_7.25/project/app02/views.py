from django.shortcuts import render,HttpResponse

# Create your views here.
# def demo01(request):
#     html="<h1> I am demo </h>"
#     print(html)
#     return HttpResponse(html)#不能直接返回html格式，要用该函数封装
def demo_html(request):
    return render(request, "login.html")
