from django.contrib import admin
from .models import InformationClassification, Tag, Topic, LinkCategory, Links, Article, Banner
# Register your models here.

admin.site.register(InformationClassification)
admin.site.register(Tag)
admin.site.register(Topic)
admin.site.register(LinkCategory)
admin.site.register(Links)
admin.site.register(Banner)

class ArticleAdmin(admin.ModelAdmin):
    list_display = ('title', 'classification', 'writeTime', 'reading', 'source', 'author')
    list_editable = ('classification',)
    list_display_links = ('title',)
    search_fields = ('title', 'classification', 'source', 'author')

admin.site.register(Article, ArticleAdmin)
