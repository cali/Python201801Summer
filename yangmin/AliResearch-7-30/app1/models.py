from django.db import models

# Create your models here.
class InformationClassification(models.Model):
    name = models.CharField(max_length=30)

    class Meta:
        verbose_name = 'Classification'
        verbose_name_plural = verbose_name
        db_table = "Classification"

    def __str__(self):
        return self.name


class Tag(models.Model):
    Tag_Choice = (
        ('观点', '观点'),
        ('知识产权', '知识产权'),
        ('报告', '报告'),
        ('电商', '电商'),
        ('金砖五国', '金砖五国'),
    )
    name = models.CharField(max_length=30, choices=Tag_Choice,default='观点')
    referenceCount = models.IntegerField()

    class Meta:
        verbose_name = 'Tag'
        verbose_name_plural = verbose_name
        db_table = "Tag"

    def __str__(self):
        return self.name


class Topic(models.Model):
    name = models.CharField(max_length=30)

    class Meta:
        verbose_name = 'Topic'
        verbose_name_plural = verbose_name
        db_table = "Topic"

    def __str__(self):
        return self.name


class LinkCategory(models.Model):
    name = models.CharField(max_length=50)

    class Meta:
        verbose_name = 'LinkCategory'
        verbose_name_plural = verbose_name
        db_table = "LinkCategory"

    def __str__(self):
        return self.name


class Links(models.Model):
    name = models.CharField(max_length=50)
    url = models.URLField(max_length=50)
    Category = models.ForeignKey(LinkCategory,related_name='Links_Category')

    class Meta:
        verbose_name = 'Link'
        verbose_name_plural = verbose_name
        db_table = "Link"

    def __str__(self):
        return self.name


from django.core.exceptions import ValidationError
def validata_price(value):
    if value < 0:
        raise ValidationError(f"{value} 必须大于等于0")

class Article(models.Model):
    title = models.CharField(max_length=100,)
    content = models.TextField()
    classification = models.ForeignKey(InformationClassification, verbose_name='分类', related_name='Article_classification')
    writeTime = models.DateField(auto_now=True)
    # ManyToManyField的relates_name字段会产生一个中间表
    tags = models.ManyToManyField(Tag, related_name='Article_tag')
    reading = models.IntegerField(validators=[validata_price,])
    link = models.ManyToManyField(Links, related_name='Article_link')
    source = models.CharField(max_length=50)
    author = models.CharField(max_length=20)
    likeNum = models.IntegerField(validators=[validata_price,])


    class Meta:
        verbose_name = 'Article'
        verbose_name_plural = verbose_name
        db_table = "Article"

    def __str__(self):
        return self.title


# class Report(models.Model):
#     pass
#
#
# class Option(models.Model):
#     pass
#
#
# class TopArticle(models.Model):
#     pass


class Banner(models.Model):
    name = models.CharField(max_length=30)
    Article = models.OneToOneField(Article, related_name='Banner_Article')
    Img = models.ImageField(upload_to='media/', default='imgs')

    class Meta:
        verbose_name = 'Banner'
        verbose_name_plural = verbose_name
        db_table = "Banner"

    def __str__(self):
        return self.name
