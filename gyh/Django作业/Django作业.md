# 1.Web服务器的功能？常见的web服务器有哪些？
WEB服务器也称为WWW(WORLD WIDE WEB)服务器，主要功能是提供网上信息浏览服务。 WWW 是 Internet 的多媒体信息查询工具，是 Internet 上近年才发展起来的服务，也是发展最快和目前用的最广泛的服务。正是因为有了WWW工具，才使得近年来 Internet 迅速发展，且用户数量飞速增长。
Web服务器是可以向发出请求的浏览器提供文档的程序。
* 服务器是一种被动程序：只有当Internet上运行其他计算机中的浏览器发出的请求时，服务器才会响应
* 最常用的Web服务器是Apache和Microsoft的Internet信息服务器（Internet Information Services，IIS）。
* Internet上的服务器也称为Web服务器，是一台在Internet上具有独立IP地址的计算机，可以向Internet上的客户机提供WWW、Email和FTP等各种Internet服务。
* Web服务器是指驻留于因特网上某种类型计算机的程序。当Web浏览器（客户端）连到服务器上并请求文件时，服务器将处理该请求并将文件反馈到该浏览器上，附带的信息会告诉浏览器如何查看该文件（即文件类型）。服务器使用HTTP（超文本传输协议）与客户机浏览器进行信息交流，这就是人们常把它们称为HTTP服务器的原因。
* Web服务器不仅能够存储信息，还能在用户通过Web浏览器提供的信息的基础上运行脚本和程序。
* 常用的web服务器：
* 
        Apache后台服务器（主要处理php及一些功能请求 如：中文url）
        Nginx 前端服务器（利用它占用系统资源少得优势来处理静态页面大量请求）
        Lighttpd图片服务器
# 2.WSGI是什么？有什么用？
* WSGI是一个服务器(Server)和应用(Application)之间通讯的接口规范。服务器端和应用端的接口被定义在PEP 3333中。 (链接：https://www.python.org/dev/peps/pep-3333/) 。 如果一个应用（框架或工具包）是根据WSGI规范来写的， 那么它可以在任何支持这个规范的服务器上运行。
* WSGI 是服务器程序与应用程序的一个约定，它规定了双方各自需要实现什么接口，提供什么功能，以便二者能够配合使用。

    WSGI 不能规定的太复杂，否则对已有的服务器来说，实现起来会困难，不利于WSGI的普及。同时WSGI也不能规定的太多，例如cookie处理就没有在WSGI中规定，这是为了给框架最大的灵活性。要知道WSGI最终的目的是为了方便服务器与应用程序配合使用，而不是成为一个Web框架的标准。

    另一方面，WSGI需要使得middleware（是中间件么？）易于实现。middleware处于服务器程序与应用程序之间，对服务器程序来说，它相当于应用程序，对应用程序来说，它相当于服务器程序。这样，对用户请求的处理，可以变成多个 middleware 叠加在一起，每个middleware实现不同的功能。请求从服务器来的时候，依次通过middleware，响应从应用程序返回的时候，反向通过层层middleware。我们可以方便地添加，替换middleware，以便对用户请求作出不同的处理。
# 3.Django的优劣势
https://blog.csdn.net/bluehawksky/article/details/50999354
# 4.Django的设计哲学
http://www.codeweblog.com/django%E7%9A%84%E8%AE%BE%E8%AE%A1%E6%80%9D%E6%83%B3/
# 5.Django的请求响应流程
http://python.jobbole.com/85203/