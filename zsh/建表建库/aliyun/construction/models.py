from django.db import models
from django.contrib.auth.models import User
from aliyun.settings import MEDIA_URL


class InformationClassification(models.Model):
    #  导航栏中每一个导航栏的名字
    Username = models.CharField(max_length=10)
    img = models.ForeignKey(banner,verbose_name="轮播图")
    # 分类的链接文章(5类)一对多
    Category_airticle = models.ForeignKey(Article)
    # 推荐的链接模块存储()一对一
    recommand = models.OneToOneField(Article)
    # 全站热门()
    ALL_topic = models.ForeignKey(Article)
    # 热门标签
    hot_topic = models.ForeignKey(Tag)
    class Meta:
        verbose_name = "导航栏中每一个模块"
        verbose_name_plural = verbose_name
        db_table = "Leading"


class Tag(models.Model):
    tag_name = models.CharField(max_length=10)
    airticle_name = models.ManyToManyField(Article,verbose_name="某标签下的文章")
    class Meta:
        verbose_name = "标签"
        verbose_name_plural = verbose_name
        db_table = "Tag"


class Linkcategory(models.Model):
    category_name = models.OneToOneField(Tag,verbose_name="分类链接")
    link_url = models.CharField(max_length=64)
    link_article = models.ForeignKey(Article,verbose_name="每一类链接下的文章")
    # link_date = models.CharField()
    # link_number = models.CharField()


class Link(models.Model):
    link_name = models.OneToOneField(Article,verbose_name="文章链接名字")
    link_url = models.CharField(max_length=64,verbose_name="文章链接")

# def add_numbers(value):
#     if value>10:
#         value=value+1
#         return value


class Article(models.Model):
    title = models.CharField(max_length=20)
    link_tag = models.ManyToManyField(Tag,verbose_name="标签")
    originate = models.CharField(max_length=64)
    time = models.DateTimeField(auto_now_add=True)
    article_comment = models.CharField(max_length=128)
    # 有人点击便增加c此数量
    Numbers = models.IntegerField(verbose_name="点击量")
    isrecomment = models.BooleanField(verbose_name="是否推荐")

    class Meta:
        verbose_name = "文章表"
        verbose_name_plural = verbose_name
        db_table = "Article_table"


class banner(models.Model):
    imge = models.ImageField(upload_to='avator/',default='/avator/default.png')
    word = models.CharField(max_length=64)
    link = models.OneToOneField(Linkcategory,verbose_name="文章链接")


class circle(models.Model):
    circle_name = models.CharField(max_length=64)
    user = models.OneToOneField(User,verbose_name="成员")


# Create your models here.
