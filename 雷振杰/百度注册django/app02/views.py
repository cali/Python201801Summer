from django.shortcuts import render, HttpResponse
from .models import UserInfo


# Create your views here.


def baidu(request):
    return render(request, 'baiduregister.html')


def baidulog(request):
    return render(request, 'login.html')


def baidu_register(request):
    print(request)
    print(request.GET)
    username = request.GET.get("username", '')
    password = request.GET.get("password", '')
    phonenum = request.GET.get("phonenum", '')
    if username == '' or password == '' or phonenum == '':
        return render(request, 'baiduregister.html',{"err":""})
    elif 7<=len(username)<=14 and 6<=len(password)<=14 and len(phonenum)==11:
        UserInfo.objects.create(username=username, password=password, phonenum=phonenum)
        userlist = UserInfo.objects.all()
        print(username)
        print(userlist)
        print(f"username:{username}, password:{password}, phonenum:{phonenum}")
        return render(request, 'baiduregister.html',
                      {"html_username": username, "html_password": password,
                       "html_phonenum": phonenum, "user_list": userlist})
    else:
       return render(request, 'baiduregister.html',{"err":'错误'})

