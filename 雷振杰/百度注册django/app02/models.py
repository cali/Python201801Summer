from django.db import models

# Create your models here.

class UserInfo(models.Model):
    username = models.CharField(max_length=64)
    password = models.CharField(max_length=64)
    phonenum = models.CharField(max_length=64)