from django.db import models

# Create your models here.

class Student(models.Model):
    #AutoField 自增型字段  =》int
    student_id = models.AutoField(primary_key=True)

    #Charfield 文本型字段  =》varchar
    #必填项：max_length :最大字符长度（max_length将在数据库层和表单验证中起作用）
    username = models.CharField(max_length=32)

    #默认的表单样式：Textare 大文本型字段
    desc = models.TextField()

    #IntegerField: 整型字段
    #默认的表单样式：TextInput.
    #字段类型从charfield -》interfield
    sex = models.IntegerField()

    #DateField: 日期型字段
    #DateTimeField:日期时间型字段
    #auto_now_add:insert的时候会更新这个值
    #auto_now:insert/update的时候会更新这个值

    #入学日期
    adminssion_date = models.DateField(default='2018-01-01')

    #录入时间
    insert_time = models.DateTimeField(auto_now_add=True)

    #更新时间
    update_time = models.DateTimeField(auto_now=True)

    #FloatField:浮点型字段
    #表单控件：TextInput
    endlish_score = models.FloatField()

    #DecimalField :Decimal型字段，精确的浮点数
    #DecimalField:max_digits:位数总数，包括小数点后的位数。该值必须大于等于decimal_places
    #DecimalField.decimal_places:小数点后的数字数量
    #学费(跟钱相关的，一定要用这个字段类型)
    schooling = models.DecimalField(max_digits=7, decimal_places=2)

    #EmailField : 邮箱字段=》varchar
    #一个 CharField用来检查输入的email地址是否合法
    #仅在表单验证时生效
    email = models.EmailField()

    #GenericIPAddressField:IP型字段
    #ipv4或ipv6
    #最后登录IP
    last_login_ip = models.GenericIPAddressField()

    #BooleanField: 不允许为NULL
    #NULLBooleanField：值可以为NULL/True/False
    #当前登录状态
    is_login = models.BooleanField()
    is_login2 = models.NullBooleanField()

    #ImageField:图片类型字段 =》图片路径
    #upload_to
    #ImageField.heght_field:该属性的设定会在模型实例保存时，自动填充图片的高度
    #ImageField.width_field:给属性的设定会在模型实例保存时，自动填充图片的宽度
    avator = models.ImageField(upload_to='avator/', default='avator/default.png')
