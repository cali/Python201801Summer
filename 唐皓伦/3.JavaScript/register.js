function click11() {
    document.getElementById("one").innerHTML = "设置后不可更改\n" +
        "                <br>" +
        "                中英文均可，最长14个英文或7个汉字";
}

function click10() {
    x = document.getElementById("username").value;
    if (x.length > 14) {
        y = document.getElementById("one").innerHTML = "<img src='error.png' style='margin: 8px auto auto 0'><p style='color: black;display: inline'>用户名不能超过14位!</p>";
    }
    else if (x == "") {
        y = document.getElementById("one").innerHTML = null;
    }
    else {
        y = document.getElementById("one").innerHTML = "<img src='ok.png' style='margin: 5px 80px auto auto'>"
    }
}

function click21() {
    document.getElementById("two").innerHTML = "请输入中国大陆手机号\n" +
        "                <br>" +
        "                其他用户不可见";
}

function click20() {
    x = document.getElementById("usernumber").value;
    if (isNaN(x) == true) {
        document.getElementById("two").innerHTML = "<img src='error.png' style='margin: 8px 0 auto auto'><p style='color: black;display: inline'>手机号格式不正确！</p>"
    }
    else if (x.length == 11) {
        document.getElementById("two").innerHTML = "<img src='ok.png' style='margin: 5px 8px auto auto;float: left'>"
    }
    else if (x == "") {
        document.getElementById("two").innerHTML = null
    }
    else {
        document.getElementById("two").innerHTML = "<img src='error.png' style='margin: 8px 0 auto auto'><p style='color: black;display: inline'>手机号位数不正确！</p>"
    }
}

function click31() {
    document.getElementById("four").id = "three";
    document.getElementById("three").innerHTML = "<ul>\n" +
        "            <li>长度为6~14个字符</li>\n" +
        "            <li>支持数字,大小写字母和标点符号</li>\n" +
        "            <li>不允许有空格</li>\n" +
        "        </ul>"
}

function click30() {
    x = document.getElementById("userpassword").value;
    if ((x.length>0 && x.length < 6 )|| x.length > 14) {
        document.getElementById("three").id = "four";
        document.getElementById("four").innerHTML = "<img src='error.png' style='margin: 5px'><p style='color: black;display: inline'>密码位数不正确！</p>"
    }
    else if (x == "") {
        document.getElementById("three").innerHTML = null
    }
    else {
        document.getElementById("three").id = "four";
        document.getElementById("four").innerHTML = "<img src='ok.png' style='margin: 5px 82px auto auto'>"
    }
}
