#导入数据库模块
from django.db import models
# Create your models here.
# 一个类对应一个表
class UserInfo(models.Model):
    # 多出id字段： 如果一个表里面没有主键，那么，它会自动生成一个名为id的主键
    username = models.CharField(max_length=10)
    iphone = models.CharField(max_length=11)
    password = models.CharField(max_length=64)
    verifyCode = models.CharField(max_length=6)