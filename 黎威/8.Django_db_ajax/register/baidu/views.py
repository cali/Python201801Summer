from django.shortcuts import render,HttpResponse
from .models import UserInfo
# Create your views here.


def register(request):
    print("="*20)
    print(request)
    print(request.GET)

    username = request.GET.get("userName")
    iphone = request.GET.get("iphone")
    password = request.GET.get("password")
    verifyCode = request.GET.get("verifyCode")
    mystr=str()
    if username and iphone and password and verifyCode:
        userlist = UserInfo.objects.all()
        for i in userlist:
            if username == i.username:
                print("存在",i.username)
                mystr='用户已存在'
        if mystr=='':
            UserInfo.objects.create(username=username,iphone=iphone,password=password,verifyCode=verifyCode)
            mystr='插入成功'
            print("成功")
    else:
        mystr='未插入任何值'
        print("空")
    userlist = UserInfo.objects.all()
    print("存在数据",userlist)
    return render(request,'baidu_register.html',{'mystr':mystr,'user_list':userlist})

def check_name(request):
    username = request.GET.get('username')
    # 返回用户是否可以注册===flag
    flag = 1
    if UserInfo.objects.filter(username=username):
        flag = 0
    # jsonp的格式被callback包装 需要分开处理
    if request.GET.get('callback'):
        funcname = request.GET.get('callback')
        data = f"{funcname}({flag})"
        return HttpResponse(data)
    return HttpResponse(flag)