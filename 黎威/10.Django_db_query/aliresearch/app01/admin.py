from django.contrib import admin
from django.utils.safestring import mark_safe
from aliresearch.settings import MEDIA_URL
from . import models

# Register your models here.


# 定制类
class ArticleAdmin(admin.ModelAdmin):
    # 列表页显示的字段
    list_display = ('article_title', 'article_information_classification',
                    'publish_date', 'view_times', 'mycol')
    # 可直接点击进入修改页面
    list_display_links = ('article_title',)
    # 可在列表页直接修改（链接和编辑不能同时用）
    list_editable = ( 'article_information_classification', )
    # 右侧过滤器
    list_filter = ('is_notice', 'is_case',  'is_recommend')
    search_fields = ('article_title', 'publish_date')
    # inlines
    # 编辑页显示/不显示哪些字段
    # fields = ( 'date_count', 'unique_vistor')
    # exclude = ( 'date_count', 'unique_vistor')
    # 编辑页分块显示
    fieldsets = (
        (None, {
            'fields': ('article_title', 'article_information_classification',
                    'article_cover', 'article_abstract', 'article_content', 'article_tag' ),
        }),
        ('高级设置', {
            'fields': ('is_notice', 'is_case', 'is_recommend', 'is_business_evaluation'),
            'classes': ('wide', 'extrapretty'),
            # 'classes': ('collapse',),
        }),
    )

    # 定义一个list页面的显示名
    def mycol(self, obj):
        return obj.article_title
    mycol.short_description = '新增列'


class ReportAdmin(admin.ModelAdmin):
    # 列表页显示的字段
    list_display = ['report_title', 'article', 'report_publisher',
                    'report_enable', 'cover_show']
    # 定义一个list页面的显示名
    def cover_show(self, obj):
        return mark_safe("<img src='{media_url}{img_url}' />".format(media_url=MEDIA_URL, img_url=obj.report_cover))
    cover_show.short_description = '封面'

admin.site.register(models.InformationClassification)
admin.site.register(models.Tag)
admin.site.register(models.Banner)
admin.site.register(models.Report, ReportAdmin)
admin.site.register(models.Topic)
admin.site.register(models.LinkCategory)
admin.site.register(models.Links)
admin.site.register(models.TopArticle)
admin.site.register(models.Option)
admin.site.register(models.Guid)
admin.site.register(models.Article, ArticleAdmin)