#### Django作业
##### 1. 实例上课的实例"/demo_html/"
##### 2. 将之前的百度注册登录写2个url并展示： "/login/", "register"
##### 3. Web服务器的功能？常见的Web服务器有哪些  
1. Web服务器的功能：
   
        Web服务器是可以向发出请求的浏览器提供文档的程序。
        1、服务器是一种被动程序：只有当Internet上运行其他计算机中的浏览器发出的请求时，服务器才会响应。
        2、最常用的Web服务器是Apache和Microsoft的Internet信息服务器（Internet Information Services，IIS）。
        3、Internet上的服务器也称为Web服务器，是一台在Internet上具有独立IP地址的计算机，可以向Internet上的客户机提供WWW、Email和FTP等各种Internet服务。
        4、Web服务器是指驻留于因特网上某种类型计算机的程序。当Web浏览器（客户端）连到服务器上并请求文件时，服务器将处理该请求并将文件反馈到该浏览器上，附带的信息会告诉浏览器如何查看该文件（即文件类型）。服务器使用HTTP（超文本传输协议）与客户机浏览器进行信息交流，这就是人们常把它们称为HTTP服务器的原因。Web服务器不仅能够存储信息，还能在用户通过Web浏览器提供的信息的基础上运行脚本和程序。
2. 常见Web服务器：
  
        最主流的三个Web服务器：Apache、Nginx、IIS
##### 4. WSGI是什么？有什么用
    WSGI（Web Server Gateway Interface）是一种规范，它定义了使用python编写的web app与web server之间接口格式，实现web app与web server间的解耦。
    也就是说，WSGI就像是一座桥梁，一边连着web服务器，另一边连着用户的应用。但是呢，这个桥的功能很弱，有时候还需要别的桥来帮忙才能进行处理。
 
    WSGI有两方：“服务器”或“网关”一方，以及“应用程序”或“应用框架”一方。
    服务方调用应用方，提供环境信息，以及一个回调函数（提供给应用程序用来将消息头传递给服务器方），并接收Web内容作为返回值。
    所谓的 WSGI中间件同时实现了API的两方，因此可以在WSGI服务和WSGI应用之间起调解作用：从WSGI服务器的角度来说，中间件扮演应用程序，而从应用程序的角度来说，中间件扮演服务器。
    
    “中间件”组件可以执行以下功能：
            重写环境变量后，根据目标URL，将请求消息路由到不同的应用对象。
            允许在一个进程中同时运行多个应用程序或应用框架。
            负载均衡和远程处理，通过在网络上转发请求和响应消息。
            进行内容后处理，例如应用XSLT样式表。
##### 5. Django的设计哲学
    主要目的是简便、快速地开发数据库驱动的网站
    功能丰富：自带ORM、URL分发、模板系统、表单处理、Cache系统、会话管理、国际化、后台管理
    可扩展：pypi及git上有很多基于Django开发的应用

##### 6. Django的优劣
1. Django优势

        完美的文档
        全套的解决方案
        强大的URL路由配置
        自助管理后台

2. Django劣势

        系统紧耦合，灵活性差

##### 7. Django请求响应流程
    1． 浏览器发送请求（基本上是字节类型的字符串）到web服务器
    
    2． web服务器（比如，Nginx）把这个请求转交到一个WSGI（比如，uWSGI），或者直接地文件系统能够取出 一个文件（比如，一个CSS文件）
    
    3．不像web服务器那样，WSGI服务器可以直接运行Python应用。请求生成一个被称为environ的Ptyhon字典， 而且，可以选择传递过去几个中间件的层，最终，达到Django应用
    
    4．URLconf中含有属于应用的urls.py选择一个视图处理基于请求的URL的那个请求，这个请求就已经变成了 HttpRequest——一个Python字典对象
    
    5．被选择的那个视图通常要做下面所列出的一件或者更多件事情：
    
        A. 通过模型与数据库对话
        B. 使用模板渲染HTML或者任何格式化过的响应
        C. 返回一个纯文本响应（不被显示的）
        D. 抛出一个异常
    
    6．HttpResponse对象离开Django后，被渲染为一个字符串
    
    7．在浏览器见到一个美化的，渲染后的web页面
##### 8. 课外阅读
* [wsgi简介](https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000/001432012393132788f71e0edad4676a3f76ac7776f3a16000)
* [wsgi官方](http://wsgi.readthedocs.io/en/latest/)
* [Web开发中三层架构是哪三层？](https://baike.baidu.com/item/%E4%B8%89%E5%B1%82%E6%9E%B6%E6%9E%84/11031448?fr=aladdin)
    
    1：数据访问层：主要看数据层里面有没有包含逻辑处理，实际上它的各个函数主要完成各个对数据文件的操作。而不必管其他操作。
    
    2：业务逻辑层：主要负责对数据层的操作。也就是说把一些数据层的操作进行组合。
    
    3：表示层：主要对用户的请求接受，以及数据的返回，为客户端提供应用程序的访问。