from django.db import models

# Create your models here.
# 文章
class Article (models.Model):
    # 标题
    article_title=models.CharField(verbose_name="标题",max_length=20)
    # 来源----可以为空
    article_source=models.CharField(verbose_name="来源",max_length=20,null=True,blank=True)
    # 分类----关系表定义

    # 时间
    article_date=models.DateTimeField(auto_now_add = True)
    # 阅读次数
    article_hits=models.IntegerField(verbose_name="阅读次数",default=0)
    # 文本
    article_desc=models.TextField(verbose_name="文本")

    class Meta:
        # verbose_name：设置Admin-表显示名（单数）
        verbose_name = '文章'
        # verbose_name_plural：设置表显示名（复数）
        verbose_name_plural = verbose_name
        # db_table：数据表名(默认名： appname_modelname)
        db_table = "app01_article_example"
        # get_latest_by：Manager的latest()和earliest()中使用的默认字段。
        # 默认是按主键排序
        get_latest_by = "article_date"
        # ordering: 对象默认的顺序，获取一个查询集时使用
        # - => 倒序
        ordering = ['-article_date', '-article_hits', ]

    def _str_(self):
        return self.article_title
    pass

# 信息分类
class InformationClassification (models.Model):
    # 消费 、小企业、产品升级、涉农、O2O、
    # 跨境、物流、云计算与大数据、微金融、
    # 宏观经济、互联网、网货、网规和立法、其他
    class_name = models.CharField(verbose_name='类名', max_length=10)
    class_article = models.ManyToManyField(Article, verbose_name="此类文章")
    class Meta:
        # verbose_name：设置Admin-表显示名（单数）
        verbose_name = '分类'
        # verbose_name_plural：设置表显示名（复数）
        verbose_name_plural = verbose_name
        # db_table：数据表名(默认名： appname_modelname)
        db_table = "app01_class_example"
    def __str__(self):
        return self.class_name
    pass

# 标签
class Tag (models.Model):
    # 观点、数据、案例、报告
    # 研究院报告、资讯、阿里商业评论
    # 淘宝村、互联网plus、互联网
    # 阿里研究院、大数据、信息经济
    # 电子商务、阿里巴巴、新经济智库大会
    # 农村电商、电商数据、云计算、物流
    tag_name= models.CharField(verbose_name='标签名', max_length=10)
    tag_article = models.ManyToManyField(Article, verbose_name="此标签文章")
    class Meta:
        # verbose_name：设置Admin-表显示名（单数）
        verbose_name = '标签'
        # verbose_name_plural：设置表显示名（复数）
        verbose_name_plural = verbose_name
        # db_table：数据表名(默认名： appname_modelname)
        db_table = "app01_Tag_example"
    def __str__(self):
        return self.tag_name
    pass

# 主题
# class Topic (models.Model):
#     pass

# 链接类别
# class LinkCategory (models.Model):
#     pass

# 链接
# class Links (models.Model):
#     pass

# 报告
# class Report (models.Model):
#     pass

# 选项
# class Option (models.Model):
#     pass

# 热门文章
# class TopArticle (models.Model):
#     pass

# 横幅
# class Banner (models.Model):
#     pass

# GUID
# class Guid (models.Model):
#     pass
