from django.contrib import admin

# Register your models here.
from .models import Article,Tag,InformationClassification
class ArticleAdmin(admin.ModelAdmin):
    # 列表页显示的字段
    list_display = ('article_title', 'article_date', 'article_hits')
    # 可在列表页直接修改（链接和编辑不能同时用）
    # list_editable = ('',)
    # 可直接点击进入修改页面
    list_display_links = ('article_title',)
    # 右侧过滤器
    list_filter = ('article_date','article_hits')
    # 查询
    search_fields =('article_title',)
    # 编辑页显示/不显示哪些字段
    # fields = ('article_title', 'article_date', 'article_hits', 'article_')
    # exclude =

class TagAdmin(admin.ModelAdmin):
    search_fields =('Tag_name',)

class ClassAdmin(admin.ModelAdmin):
    search_fields=('class_name',)

admin.site.register(Article, ArticleAdmin)
admin.site.register(Tag,TagAdmin)
admin.site.register(InformationClassification,ClassAdmin)