1.web服务器的功能？常见的Web服务器有哪些

答：功能是提供网上信息浏览服务，常见的web服务器：Apache,Lighttpd,Tomcat,IBM WebSphere,nginx

2.WSGI是什么？有什么用

答：PythonWeb服务器网关接口（Python Web Server Gateway Interface，缩写为WSGI)是Python应用程序或框架和Web服务器之间的一种接口

WSGI 没有官方的实现, 因为WSGI更像一个协议. 只要遵照这些协议,WSGI应用(Application)都可以在任何服务器(Server)上运行, 反之亦然。

3.Django的设计哲学

松耦合，少写代码，开发速度快，不要重复，一致性高

4.Django的优劣

  1、优点

        1、开源框架，有完美的文档支持

        2、解决方案众多，内部功能支持较多

        3、优雅的URL，完整的路由系统

        4、自助式的后台管理
  2、缺点

        目标：高内聚，低耦合

            1、耦合度偏高

            内聚：尽量让模块功能单一化，方便使用和调用

            耦合：模块之间的依赖关系

5.Django的请求响应流程

1.用户浏览器一个url，发起一个请求

2.生成一个WSGI实例

3、构造WSGIRequest。

4、 处理Middleware的request中间件

5、 URLConf通过urls.py文件和请求的URL找到相应的视图函数

6、 开始调用View中相应的视图函数或基于类的视图。

7、View进行一些处理，如通过模型Models返回数据。

8、如果需要，Views可以创建一个额外的Context，Context被当做变量传给Template。

9、Template渲染输出

10、渲染后的输出被返回到View

11、HTTPResponse被发送到Response Middlewares

12、Response Middlewares对response进行特定的处理，然后返回一个新的response

13、Response返回呈现给用户

14、一旦 middleware完成了最后环节，处理器将发送一个信号 request_finished，订阅这个信号的事件会清空并释放任何使用中的资源。比如，Django 的 request_finished 的订阅者subscriber会关闭所有数据库连接。

















