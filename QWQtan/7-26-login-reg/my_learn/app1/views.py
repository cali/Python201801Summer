from django.shortcuts import render,HttpResponse
from app1.models import UserInfo
from django.http import HttpResponseRedirect
from django.forms.models import model_to_dict

# Create your views here.

#视图函数 用来处理http请求的
#request:浏览器发送过来的数据
def demo01(request):
    print(request)
    html = "<h1>I am demo,tan<h1>"
    print(html)
    #返回必须返回一个HttpResponse对象
    return HttpResponse(html)

def demo_html(request):
    #html模板名是一个字符串==>str
    # print(request)
    # print(request.GET)
    #得到html中name为name的
    username = request.GET.get("name"," ")
    #获取name为passwd的如果为空就获取到abc
    passwd = request.GET.get("passwd"," ")

    if username and passwd:
        # print(f"email:{username},passwd:{passwd}")
        #跟数据交互
        #插入信息
        UserInfo.objects.create(email=username,passwd=passwd)
        userlist=UserInfo.objects.all()
        print(userlist)
        return render(request,"demo_html.html",{"username":username,"passwd":passwd})
    else:
        return render(request,"demo_html.html",{"error":"请输入账号和密码"})


def login(request):
    if request.method=="POST":
        username = request.POST.get("name"," ")
        passwd = request.POST.get("passwd"," ")
        print(username,passwd)
        if username and passwd:
            user = UserInfo.objects.filter(email=username)
            # print(user)
            for i in user:
                if i.email==username and i.passwd == passwd:
                    print("登录成功")
                    return render(request,"my_result.html",{"my_result":"登录成功"})
                else:
                    print("登录失败")
                    return render(request, "my_result.html", {"my_result": "登录失败"})
    else:
        return render(request, "login.html")
    return render(request, "login.html")


def reg(request):
    request.method="POST"
    username =request.POST.get("username","")
    passwd =request.POST.get("passwd","")
    user = UserInfo.objects.filter(email=username)
    for i in user:
        print("该用户存在")
        return render(request,'my_result.html',{"my_result":"{}已经存在".format(i.email)})
    if username and passwd:
        UserInfo.objects.create(email=username,passwd=passwd)
        print("用户添加完成")
        return render(request, 'my_result.html', {"my_result": "{}注册成功".format(username)})
    print("没有做什么操作")
    # if user==None:
    #     print('kong')
    # user_all =UserInfo.objects.all()
    # print(user_all)
    # for i in user_all:
    #     print(i)
    #     print(i.email,i.passwd)
    # if username and passwd:
    #     if user in user_all:
    #         print(user.email)
    #         if user.email == username:
    #             print("{}已经存在".format(username))
    #             return render(request,"my_result.html",{"my_result":"{}已经存在".format(username)})
    #         else:
    #             UserInfo.objects.create(email=username,passwd=passwd)
    #             print("{}添加成功".format(username))
    #             return render(request, "my_result.html", {"my_result": "{}添加成功".format(username)})
    return render(request,'reg.html')

