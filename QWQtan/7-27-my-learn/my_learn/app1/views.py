from django.shortcuts import render,HttpResponse
from app1.models import UserInfo
from django.http import HttpResponseRedirect
from django.forms.models import model_to_dict

# Create your views here.

#视图函数 用来处理http请求的
#request:浏览器发送过来的数据
def demo01(request):
    print(request)
    html = "<h1>I am demo,tan<h1>"
    print(html)
    #返回必须返回一个HttpResponse对象
    return HttpResponse(html)

def demo_html(request):
    #html模板名是一个字符串==>str
    # print(request)
    # print(request.GET)
    #得到html中name为name的
    username = request.GET.get("name"," ")
    #获取name为passwd的如果为空就获取到abc
    passwd = request.GET.get("passwd"," ")

    if username and passwd:
        # print(f"email:{username},passwd:{passwd}")
        #跟数据交互
        #插入信息
        UserInfo.objects.create(email=username,passwd=passwd)
        userlist=UserInfo.objects.all()
        print(userlist)
        return render(request,"demo_html.html",{"username":username,"passwd":passwd})
    else:
        return render(request,"demo_html.html",{"error":"请输入账号和密码"})


def login(request):
    if request.method=="POST":
        username = request.POST.get("name"," ")
        passwd = request.POST.get("passwd"," ")
        print(username,passwd)
        if username and passwd:
            user = UserInfo.objects.filter(email=username)
            user1 = UserInfo.objects.filter(email=username).values("email");
            user2 = UserInfo.objects.all().values_list("email")
            user3=UserInfo.objects.all().values();
            print(user1,user2,user3)
            # print(dir(user))
            # print(user)
            for i in user:
                if i.email==username and i.passwd == passwd:
                    print("登录成功")
                    return render(request,"my_result.html",{"my_result":"登录成功"})
                else:
                    print("登录失败")
                    return render(request, "my_result.html", {"my_result": "登录失败"})
    else:
        return render(request, "login.html")
    return render(request, "login.html")


def reg(request):
    request.method="POST"
    username =request.POST.get("username","")
    passwd =request.POST.get("passwd","")
    user = UserInfo.objects.filter(email=username)
    if not user:
        print("不存在！")
    for i in user:
        print("该用户存在")
        return render(request,'my_result.html',{"my_result":"{}已经存在".format(i.email)})
    if username and passwd:
        UserInfo.objects.create(email=username,passwd=passwd)
        print("用户添加完成")
        return render(request, 'my_result.html', {"my_result": "{}注册成功".format(username)})
    print("没有做什么操作")
    return render(request,'reg.html')


def check_form(request):
    back_data=0
    username = request.GET.get("username");
    # print(username)
    username_sql = UserInfo.objects.filter(email=username)
    if username_sql:
        # print("是json传递过来的")
        back_data=1
    username_jsonp=request.GET.get("back_jsonp");
    if username_jsonp:
        # print("jsonp?")
        # print(username_jsonp)
        data=f"{username_jsonp}({back_data})"
        return HttpResponse(data)

    return HttpResponse(back_data)


