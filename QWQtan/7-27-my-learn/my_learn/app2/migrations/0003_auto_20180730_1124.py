# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-30 03:24
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app2', '0002_auto_20180730_1113'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='student1',
            options={'get_latest_by': 'english_score', 'ordering': ['-student_id'], 'verbose_name': '学生表01', 'verbose_name_plural': '学生表01'},
        ),
        migrations.AlterField(
            model_name='student1',
            name='sex',
            field=models.IntegerField(choices=[(0, '男生'), (1, '女生'), (None, '保密')], verbose_name='性别'),
        ),
    ]
