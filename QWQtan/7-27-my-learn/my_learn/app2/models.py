from django.db import models


# Create your models here.

# Student是django.db.modesl.Model的一个子类
class Student(models.Model):
    # username是一个属性，即是一个字段
    # django字段名中不能用连续的多个下划线，例如username__id_hello
    student_id = models.AutoField(verbose_name="学号", primary_key=True, null=False)
    username = models.CharField(max_length=128, verbose_name="姓名")
    sex = models.CharField(max_length=1, verbose_name="性别")  # 0表示男生 1表示女生

    desc = models.TextField()

    # 时间类型
    # DateField:日期型字段
    # DateTimeField:日期时间型字段
    # auto_now_add:insert的时候会更新这个字段
    # auto_now:insert/update的时候会更新这个值
    # 入学时间
    adminssion_date = models.DateField(default="2018-01-01")

    # 录入时间
    insert_time = models.DateTimeField(auto_now_add=True)

    # 信息更新时间
    update_time = models.DateTimeField(auto_now=True)

    # FloatField:浮点型类型
    # 表单控件：TextInput
    # 英语成绩？
    english_score = models.FloatField()

    # DecimalField：Decimal型字段
    # DecimalField.max_digits:总位数
    # DecimalField.decimal_places小数位
    # 学费
    schooling = models.DecimalField(max_digits=6, decimal_places=2)

    # EmailField:邮箱字段
    # 一个CharField用来检查输入的email地址是不是合法
    email = models.EmailField()

    # 最后一次登录的ip
    last_login_ip = models.GenericIPAddressField()

    # 最近登录的ip
    is_login = models.BooleanField()
    # 可选择为空的ip
    is_login1 = models.NullBooleanField()

    # 这个为图片
    avator = models.ImageField(upload_to=None)


from django.core.exceptions import ValidationError


def check_score(value):
    if value < 0 or value > 100:
        raise ValidationError(f"成绩必须在0-100之间")


class Student1(models.Model):
    student_id = models.AutoField(verbose_name='学号', primary_key=True, null=False)
    username = models.CharField(verbose_name="姓名", max_length=128, db_column="username")
    sex = models.CharField(verbose_name='性别', max_length=1)
    # choice选项：
    sex_choice = {
        (0, "男生"),
        (1, "女生"),
        (None, "保密")
    }
    sex = models.IntegerField(verbose_name='性别', choices=sex_choice)
    # blank => 作用表单提交
    # null => 作用于数据库
    # 一般都是设置为True或者False
    desc = models.TextField(blank=False, null=True)
    admission_date = models.DateField(verbose_name="日期", default='2018-01-01')
    update_time = models.DateTimeField(verbose_name='更新日期', auto_now=True)
    english_score = models.FloatField(verbose_name='英语成绩', validators=[check_score, ])
    schooling = models.DecimalField(verbose_name='学费', max_digits=6, decimal_places=2, default=7800.00)
    email = models.EmailField(verbose_name='Email')
    last_login_ip = models.GenericIPAddressField(verbose_name="最后一次的登录IP", )
    is_login = models.BooleanField()
    is_login1 = models.NullBooleanField()

    # editable:是否会出现
    is_editedable = models.IntegerField(editable=False, null=True, blank=True)

    # 设置唯一索引
    # unique_for_data表示时间作为唯一检索
    # 如果表中已经存在数据时，要特别注意
    is_unique = models.CharField(verbose_name='唯一索引', max_length=10, unique=True)

    # error_messages:让你重写默认抛出的错误信息
    class Meta:
        # verbose_name:设置Admin-表显示名（单数）
        verbose_name = "学生表01"
        # verbose_name_plural:设置表显示名（复数）
        verbose_name_plural = verbose_name
        # 改变数据库的表名
        db_table = "app02_student_example"
        # get_latest_by:Manager的laters()和earliest()中使用的默认字段
        # 默认是按主机排序
        get_latest_by = "english_score"

        # ordering:对象默认的顺序，获取一个查询集时使用
        # - => 倒序
        # ordering = ['username','-student_id']
        ordering = ['-student_id']
        # unique_together:用来设置的不重复的字段组合
        # 考勤：username+date+type
        # index_together:用来设置带有索引关系组合





    def __str__(self):
        return self.username

    # save() => 保存信息
    # 重写save的目的
    def save(self, *args, **kwargs):
        # # 不保存数据则直接return
        # return
        print("student保存前")
        if self.sex == 0:
            self.username = self.username + "帅哥"
        elif self.sex == 1:
            self.username = self.username + "美女"
        super().save(*args, **kwargs)
        print("student保存后")



# 模型关系处理

# 跨文件的模型
from django.contrib.auth.models import User


# 多对一的关系：django.db.models.ForeignKye

class Deparment(models.Model):
    # verbose_name 如果是第一个字段可以不填
    name = models.CharField("任课", max_length=10)
    # 多对一：一门课只能一个老师上，一个老可以上多门课
    teacher = models.ForeignKey(User, verbose_name='任课老师',related_name='course_teacher_user')
    #一对一: 一个班只能有一个班长，一个班长只能在一个班任职
    monitor = models.OneToOneField(User,verbose_name='课代表',related_name='course_monitor_user')

    class Meta:
        verbose_name = "教师任职表"
        verbose_name_plural = verbose_name


    def __str__(self):
        return  f"{self.teacher}的{self.name}"


#  多对多关系: ManyToManyField
class Student02(models.Model):
    student_name = models.CharField(verbose_name='学生姓名', max_length=10)
    cousrses = models.ManyToManyField(Deparment, verbose_name="我的课程")


    #__str__
    #__repr__
    def __str__(self):
        return self.student_name

    class Meta:
        verbose_name = "学生列表"
        verbose_name_plural = verbose_name


    def save(self,*args,**kwargs):
        print("student保存前")
        # if self.student_name.sex

        super().save(*args,**kwargs)
        print("student保存后")


#  多对多关系: ManyToManyField
class Student03(models.Model):
    student_name = models.OneToOneField(Student1,verbose_name='学生姓名', max_length=10)
    cousrses = models.ManyToManyField(Deparment, verbose_name="我的课程")


    #__str__
    #__repr__


    class Meta:
        verbose_name = "学生_选课列表"
        verbose_name_plural = verbose_name


    def save(self,*args,**kwargs):
        print("student保存前")
        # if self.student_name.sex
        if  self.student_name.sex==0:
            self.student_name.username = self.student_name.username+"帅哥"
        if self.student_name.sex==1:
            self.student_name.username=self.student_name.username+"美女"
        super().save(*args,**kwargs)
        print("student保存后")

    # 实例方法
    def myname(self):
        print(self.student_name.username)

    def __str__(self):
        print(self.student_name.username)
        return self.student_name.username
