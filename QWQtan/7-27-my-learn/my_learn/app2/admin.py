from django.contrib import admin

# Register your models here.


from  .models import Student,Student1,Deparment,Student03


class Student1A(admin.ModelAdmin):
    #列表里面显示的字段
    list_display = ("username","sex","english_score")
    # #可以在列表页直接修改
    list_editable = ('sex',)
    # #可以直接进入修改页面
    list_display_links = ("username",)
    # 右侧过滤器
    list_filter = ('sex','english_score')

    search_fields = ('username', 'english_score')


admin.site.register(Student)
admin.site.register(Student1,Student1A)
admin.site.register(Deparment)
admin.site.register(Student03)
