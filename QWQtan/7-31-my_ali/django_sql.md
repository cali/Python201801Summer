#1.在Author表中插入4条数据（用2种方法插入）

	1.author1 =Author.objects.create(name="tan4",email="34@qq.com")
	2.author1 =Author(name="mo1",email="3423@qq.com")
	  author1.save()
	3.blog = Blog.objects.create(name="研究莫魁1",tagline="莫魁是个奥特曼1")
#3修改id为1的Author的用户名为 "cali_1"
	1.author1 =Author.objects.get(id=2)
 	  author1.name = "cali_1"
	  author1.save()
	2. Author.objects.filter(id=2).update(name="18kui")

#添加一条数据
	1.mokui123=Author.objects.create  (name="mokui1",email="232@qq.com")
	  entry1=Entry.objects.get(id=2)
	  entry1.authors.add(mokui123)

#删除一条数据
	blog = Blog.objects.get(id=5)
	blog.delete()


#7. 将Entry表中id为3的记录的Author修改为Author表中id=1、2对应的作者
	author1=Author.objects.get(id=2)
	author2=Author.objects.get(id=3)
	entry=Entry.objects.get(id=2)
	entry.authors.add(author1,author2)

#8. 删除Entry表中id为1的记录的blog外键
	entry = Entry.objects.get(id=2)
	entry.blog=None



#9批量更新 Author表中email的字段值为"sc@qq.com"
	author = Author.objects.all().update(email="sc@qq.com")


 #10删除Author表中所有记录
	author = Author.objects.all().delete()

#1. 根据文章id查询文章数据
	Article.objects.get(id=1)

#2. 根据标签id查询该分类的所有文章
	Tag.objects.get(id=1).article_set.all()	
	
#3. 全站热门：按点击排序，找到点击量最高的前5篇文章
	Article.objects.all().order_by('-view_times')[0:5]
#4. 本类热门：查找指定类别id中，找到点击量最高的前5篇文章
	InformationClassification.objects.get(id=1).article_set.all().order_by('-view_times')[0:5]

#5. 找出最近发表的的10篇文章
	Article.objects.all().order_by('-publish_date')[0:10]
