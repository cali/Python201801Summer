from django.shortcuts import render
from  django.shortcuts import  HttpResponse
#编写业务处理逻辑
# Create your views here.

#request 必填，名字可以修改，封装了用户的请求的所有内容 是request是浏览器发送过来的数据
def demo01(request):
    print(request)
    html = '<h1>i am demo01</h1>'
    print(html)
    #此处不能直接return html ，必须放回一个HttpResponse对象
    return  HttpResponse(html)

def demo_html(request):

    # html模板名 str形式
    return render(request,'demo_html.html')
# 登录函数


def login(request):

    # html模板名 str形式
    return  render(request,'login.html')


# 注册函数
def register(request):

    # html模板名 str形式
    return  render(request,'test_register.html')


