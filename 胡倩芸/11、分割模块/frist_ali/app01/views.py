from django.shortcuts import render

# Create your views here.

def ali_detail(request):
    return render(request, 'ali_detail.html')

def ali_index(request):
    return render(request, 'ali_index.html')

def alisub(request):
    return render(request, 'alisub.html')

def first(request):
    return render(request, 'first.html')
