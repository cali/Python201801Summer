from django.db import models

# Create your models here.

# 工程项目信息分类体系
class InformationClassification(models.Model):
    research_name = models.CharField(verbose_name="研究领域分类",max_length=10,)
    research_id = models.AutoField(primary_key=True)

#标签、名称
class Tag(models.Model):
    tag_name = models.CharField(verbose_name="标签名字", max_length=10, )
    tag_id = models.AutoField(primary_key=True)
    research_id = models.ForeignKey(InformationClassification)

# 主题
class Topic(models.Model):
    topic_name = models.CharField(verbose_name="主题名字", max_length=10, )
    topic_id = models.AutoField(primary_key=True)
    tag_id = models.ForeignKey(Tag)




#链接种类
class LinkCategory(models.Model):
    pass

# 链接
class Links(models.Model):

    title = models.CharField(verbose_name='标题', max_length=15)
    text_part = models.TextField(verbose_name='正文部分')
    topic_image = models.CharField(verbose_name='图片', max_length=200)
    post_date = models.DateTimeField(verbose_name='发表日期')
    topic_name = models.CharField(verbose_name='话题名',max_length=10)
    link_url = models.CharField(verbose_name='链接地址', max_length=100)

#文章
class Article(models.Model):
    article_title = models.CharField(verbose_name='文章标题',max_length=100)
    source = models.CharField(verbose_name='来源',max_length=10)
    classify = models.CharField(verbose_name='分类',max_length=20)
    post_time = models.DateTimeField(verbose_name='发表时间')
    contents = models.TextField(verbose_name='文章内容')
    visitor_volume = models.IntegerField(verbose_name='阅读量')

class Report(models.Model):
    pass

#选择
class Option(models.Model):
    pass

#置顶文章
class TopArticle(models.Model):
    pass

# 标语
class Banner(models.Model):
    pass

# 唯一标识符
class Guid(models.Model):
    pass


