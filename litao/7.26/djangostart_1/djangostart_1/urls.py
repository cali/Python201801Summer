"""djangostart_1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

#从app02导入模块并取别名为app02_views
from app02 import views as app02_views
#注意：在Django在，所有模块、函数的导入的根路径就是项目的根目录


urlpatterns = [
    #admin 后台路由
    url(r'^admin/', admin.site.urls),
    #自定义路由
    url(r'^demo01/',app02_views.demo01),
    url(r'^demo02/$',app02_views.demo02),
    url(r'^regist/$',app02_views.regist),
    url(r'^login/$',app02_views.login),
    url(r'^demo_form_reg/$',app02_views.demo_form_reg),

]
