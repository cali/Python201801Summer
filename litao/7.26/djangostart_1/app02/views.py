from django.shortcuts import render,HttpResponse

import re
# Create your views here.

#试图函数,处理http请求的
#request:是浏览器发送过来的数据
def demo01(request):
    print(request)
    html = "<h1> i am demo </h1>"
    print(html)
    return HttpResponse(html)
    #必须返回一个HttpResponse对象

def demo02(request):
    #HTML模板名：str
    return render(request,'demo02.html')

from .models import Users

def name_check(name):
    if len(name)<6 or len(name)>16:
        name=0

def phone_check(phone):
    phone_pat = re.compile('^(13\d|14[5|7]|15\d|166|17[3|6|7]|18\d)\d{8}$')
    res = re.search(phone_pat,phone)
    if res:
        phone=phone
    else:
        phone=0
def password_check(password):
    if password.isalpha()==False:
        password=0

def regist(request):
    print(request)
    print(request.GET)
    name = request.GET.get("name","admin")
    if len(name)<6 or len(name)>16:
        name=0

    phone = request.GET.get("phone","13131313131")
    phone_pat = re.compile('^(13\d|14[5|7]|15\d|166|17[3|6|7]|18\d)\d{8}$')
    res = re.search(phone_pat, phone)
    if res:
        phone = phone
    else:
        phone = 0

    password = request.GET.get("password","123456")
    if password.isalpha()==False:
        password=0

    if name!=0 or phone!=0 or password!=0:
        Users.objects.create(name=name,phone=phone,password=password)
        userlist = Users.objects.all()
        print(userlist)
        print(f"name:{name},phone:{phone},password:{password}")
        return render(request,'regist.html',{"html_name":name,"html_phone":phone,"html_password":password,"user_list":userlist})
    else:
        return render(request,'regist.html')
def login(request):
    return render(request,'login.html')

from .models import UserInfo

def demo_form_reg(request):
    #HTML模板名：str
    print(request)
    #如果你是用GET方法提交，用request.GET获取数据
    #如果你是用POST方法提交，用reques.POST获取数据

    print(request.GET)
    #如果key不存在，会有keyerror
    email = request.GET.get("email","123@qq.com")
    #如果key不存在，会返回一个默认值（none）
    password = request.GET.get("password","abc")

    #跟数据库交互
    #插入记录
    UserInfo.objects.create(email=email,password=password)
    #查询数据库中的所有信息
    userlist = UserInfo.objects.all()
    print(userlist)
    print(f"email:{email},password:{password}")
    #render的第三个参数，是一个字典
    #html_email , html_pasword 是在HTML页面上使用的名字
    #email/password是变量
    return render(request,'demo_form_reg.html',{"html_email":email,"html_password":password,"user_list":userlist})