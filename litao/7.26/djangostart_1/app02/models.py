from django.db import models

# Create your models here.

#一个类对应一个表
class UserInfo(models.Model):
    #多出id字段：如果表里面没有主键，那么，它会自动生成一个名为id的主键
    email = models.EmailField()
    password = models.CharField(max_length=64)

class Users(models.Model):
    name = models.CharField(max_length=16)
    phone = models.CharField(max_length=11)
    password = models.CharField(max_length=64)