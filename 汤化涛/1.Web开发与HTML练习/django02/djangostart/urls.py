"""djangostart URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
#导入模块
from app01 import views as app01_views
from wolf import views as wolf_views
from testapp import views as testapp_views
from register import views as register_views
urlpatterns = [
    #admin后台路由
    url(r'^admin/', admin.site.urls),
    #自定义路由
    url(r'^demo01/$', app01_views.demo01),
    url(r'^sky/$', wolf_views.sky),
    url(r'^testapp/$', testapp_views.testapp),
    url(r'^register/$', register_views.register),
]
