from django.db import models

# Create your models here.
class UserInfo(models.Model):
    name = models.CharField(max_length=88)
    phone = models.CharField(max_length=88)
    password = models.CharField(max_length=88)
    yzm = models.CharField(max_length=88)
    checkbox = models.CharField(max_length=88)