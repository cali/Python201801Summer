1、什么是AJAX，为什么要使用AJAX？
     答：AJAX = Asynchronous JavaScript and XML（异步的 JavaScript 和 XML）。AJAX 不是新的编程语言，而是一种使用现有标准的新方法。
2、AJAX最大的特点是什么？
     答：AJAX 最大的优点是在不重新加载整个页面的情况下，可以与服务器交换数据并更新部分网页内容。
3、AJAX都有哪些优点和缺点？
     答：一 优点：
            与传统的Web应用不同，Ajax在用户与服务器之间引入一个中间媒介（Ajax引擎），从而消除了网络交互过程中的处理——等待——处理——等待的缺点，从而大大改善了网站的视觉效果。
            下面我们就来看看使用Ajax的优点有哪些。
            （1）可以把一部分以前由服务器负担的工作转移到客户端，利用客户端闲置的资源进行处理，减轻服务器和带宽的负担，节约空间和成本。
            （2）无刷新更新页面，从而使用户不用再像以前一样在服务器处理数据时，只能在死板的白屏前焦急的等待。Ajax使用XMLHttpRequest对象发送请求并得到服务器响应，在不需要重新载入整个页面的情况下，就可以通过DOM及时将更新的内容显示在页面上。
            （3）可以调用XML等外部数据，进一步促进页面显示和数据的分离。
            （4）基于标准化的并被广泛支持的技术，不需要下载插件或者小程序，即可轻松实现桌面应用程序的效果。
            （5）Ajax没有平台限制。Ajax把服务器的角色由原本传输内容转变为传输数据，而数据格式则可以是纯文本格式和XML格式，这两种格式没有平台限制。
        二 缺点:
            同其他事物一样，Ajax也不尽是优点，它也有一些缺点，具体表现在以下几个方面。
            （1）大量的JavaScript，不易维护。
            （2）可视化设计上比较困难。
            （3）打破“页”的概念。
            （4）给搜索引擎带来困难