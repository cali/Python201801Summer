from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.
from .models import UserInfo
from .form import RegisterForm
from django.views.generic import View
from django.contrib.auth.hashers import make_password


# def register(request):
#     return render(request, 'my_ifram2百度注册.html')
#
#
def if_register(request):
    return render(request, '仿百度注册.html')


class Register(View):
    def get(self, request):
        return render(request, 'my_ifram2百度注册.html')

    def post(self, request):
        register_form = RegisterForm(request.POST)
        if register_form.is_valid():
            username = request.POST.get('username', '')
            if UserInfo.objects.filter(username=username):
                return render(request, '仿百度注册.html', {'msg': '该用户名已注册！'})
            else:
                password = request.POST.get('password', '')
                telphone = request.POST.get('telphone', '')
                user = UserInfo()
                user.username = username
                user.password = make_password(password)
                user.telphone = telphone
                user.save()
                return render(request, '仿百度注册.html', {'msg': '注册成功！'})
        else:
            return render(request, '仿百度注册.html', {'register_form': register_form})
