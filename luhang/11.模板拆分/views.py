from django.shortcuts import render
from django.views.generic import View
# Create your views here.


class Index(View):
    def get(self, request):
        return render(request, 'ali_index.htm')


class Detail(View):
    def get(self, request):
        return render(request, 'ali_detail.html')


class Sub(View):
    def get(self, request):
        return render(request, 'alisub.html')