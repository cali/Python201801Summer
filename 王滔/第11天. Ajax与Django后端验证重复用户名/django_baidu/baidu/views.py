from django.shortcuts import render
from django.http import JsonResponse
from .models import BaiduUser
# Create your views here.

def baidu_register(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    if len(str(password)) >= 8 and not BaiduUser.objects.filter(username=username):
        BaiduUser.objects.create(username=username, password=password)
        return render(request, 'baidu_login.html', {'html_ok': username})
    else:
        return render(request, 'baidu_register.html', {'html_err': username})

def baidu_login(request):
    return render(request, 'baidu_login.html')

def check_name(request):
    flag = 1
    username = request.GET.get('username')
    if BaiduUser.objects.filter(username=username):
        flag = 0
    return JsonResponse(flag,safe=False)