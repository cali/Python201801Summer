# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-07-26 09:41
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BaiduUser',
            fields=[
                ('username', models.CharField(max_length=8, primary_key=True, serialize=False)),
                ('password', models.CharField(max_length=64)),
            ],
        ),
    ]
