from django.db import models

# Create your models here.
class BaiduUser(models.Model):
    username = models.CharField(primary_key=True, max_length=8)
    password = models.CharField(max_length=64)