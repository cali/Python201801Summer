from django.shortcuts import render
from .models import BaiduUser
# Create your views here.

def baidu_register(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    if len(str(password)) >= 8 and not BaiduUser.objects.filter(username=username):
        BaiduUser.objects.create(username=username, password=password)
        return render(request, 'baidu_register.html', {'html_ok': username})
    else:
        return render(request, 'baidu_register.html', {'html_err': username})

