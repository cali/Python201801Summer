/**
 * Created by Administrator on 2018/7/25 0025.
 */
var flag = false;
        function focus_username()
        {
            var nameObj = document.getElementById("result_name");
            nameObj.innerHTML = "设置后不可更改，" +
                "最长14个英文或7个汉字";
            nameObj.style.color="darkgray";
        }
        function focus_passwd()
        {
            var nameObj = document.getElementById("result_pass");
            nameObj.innerHTML = "<p>长度为6-14个字符</p>" +
                "<p>支持数字，大小写字母和标点符号</p>" +
                "<p>不允许有空格</p>";
            nameObj.style.color="darkgray";
        }
        function focus_tele()
        {
            var nameObj = document.getElementById("result_tele");
            nameObj.innerHTML = "请输入中国大陆手机号，" +
                "其他用户不可见";
            nameObj.style.color="darkgray";
        }
        function blur_username()
        {
            var nameObj = document.getElementById("result_name");
            var userflag = checkusername();
            if (userflag === "1")
            {
                nameObj.innerHTML = "<img src='/static/imgs/ok_small.png'>";
            }
            else
            {
                nameObj.innerHTML = "<img src='/static/imgs/err_small.png'>";
            }

        }
        function checkusername() {
            var nameinput = document.getElementById("usna").value
            l = nameinput.length;
            if (  l <= 14 && l >= 1  ) {
                return "1";
            }
            else {
                return "0";
            }
        }

        function blur_telephone()
        {
            var nameObj = document.getElementById("result_tele");
            var userflag = checktelephone();
            if (userflag === "1")
            {
                nameObj.innerHTML = "<img src='static/imgs/ok_small.png'>";
            }
            else
            {
                nameObj.innerHTML = "<img src='/static/imgs/err_small.png'>";
            }

        }
        function checktelephone() {
            var nameinput = document.getElementById("tele").value

            if (  (/^1[34578]\d{9}$/.test(nameinput)) ) {
                return "1";
            }
            else {
                return "0";
            }
        }

        function blur_passwd()
        {
            var nameObj = document.getElementById("result_pass2");
            var nameObj2 = document.getElementById("result_pass");
            var passflag = checkpassword();
            if (passflag === "1")
            {
                nameObj.innerHTML = "<img src='/static/imgs/ok_small.png'>";
            }
            else
            {
                nameObj.innerHTML = "<img src='/static/imgs/err_small.png'>";
            }
            nameObj2.innerHTML = " "

        }
        function checkpassword() {
            var passinput = document.getElementById("pass").value;
            l = passinput.length;
            if (  l <= 14 && l >= 6 && (!/\s/.test(passinput))) {
                return "1"
            }
            else {
                return "0";
            }

            }