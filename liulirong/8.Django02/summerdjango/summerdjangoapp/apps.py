from django.apps import AppConfig


class SummerdjangoappConfig(AppConfig):
    name = 'summerdjangoapp'
