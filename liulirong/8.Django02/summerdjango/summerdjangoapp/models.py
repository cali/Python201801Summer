from django.db import models

# Create your models here.
# 一个类对应一个表
# a前提将app添加到setting中
class userinfo(models.Model):
    # 如果一个表没有主键，会自动生成名为id的主键
    email = models.EmailField()
    passwd = models.CharField(max_length=64)
class registelist(models.Model):
    username = models.CharField(max_length=10)
    tel = models.CharField(max_length=11)
    passwd = models.CharField(max_length=14)
    test = models.CharField(max_length=4)
