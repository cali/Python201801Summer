from django.shortcuts import render,HttpResponse
from .models import userinfo
from .models import registelist
# 视图函数，处理http请求
# request：浏览器发送的数据
def master(request):
    html = "<h1>I am master</h1>"
    print(request)
    print(html)
    return HttpResponse(html)
def demo(request):      # 默认从templates目录查找
    email = request.GET.get('email')    # 如果key不存在，会有keyerror
    passwd = request.GET.get('passwd') #如果key不存在，会返回一个默认值，NONE，第一个参数是key值，第二个是当不存在该key时的返回值
    print(request)  # 将请求数据打印在命令行
    print(request.GET)  # 获取请求数据中以GET方式提交表单的数据
    print(f"email:{email},password:{passwd}")
    # 跟数据库交互，插入记录
    if email and passwd:
        # create是将前端输入的数据存入数据库
        userinfo.objects.create(email=email,passwd=passwd)
        # 获取数据库中所有数据
        userlist = userinfo.objects.all()
        # render第三个参数是一个字典，用来给模板传数据
        # html_email和html_passwd是html页面使用的名字
        # return render(request,'demo.html',{"html_email":email,"html_passwd":passwd})
        return render(request,'demo.html',{"html_email":email,"html_passwd":passwd,"user_list":userlist})
    else:
        return render(request,'demo.html',{'error_msg':'邮箱和密码为空'})
def login(request):
    return render(request,'login.html')
def registe(request):
    username = request.GET.get('username')
    tel = request.GET.get('tel')
    passwd = request.GET.get('passwd')
    test = request.GET.get('test')
    if username and tel and passwd and test:
        userlist = registelist.objects.all()
        for user in userlist:
            if username == user.username:
                return render(request, 'registe.html', {"failed": "重名，注册失败"})
        if (len(username)>6 and len(username)<10) and len(tel) == 11 and (len(passwd)>6 and len(passwd)<14):
            registelist.objects.create(username=username, tel=tel, passwd=passwd, test=test)
            return render(request, 'registe.html',{"success":"注册成功"})
        else:
            return render(request, 'registe.html', {"failed": "长度不符合标准，注册失败"})
    else:
        return render(request, 'registe.html')


