from django.db import models
from aliresearch.settings import MEDIA_URL
from django.contrib.auth.models import User
# Create your models here.

class Nav(models.Model):
    nav_content = models.CharField(max_length=20)
    class Meta:
        verbose_name = '导航栏'
        verbose_name_plural = verbose_name
    def __str__(self):
        return self.nav_content


class Catetory(models.Model):
    category_content = models.CharField(max_length=20)
    hot = models.ForeignKey(Article,verbose_name='本类热门')
    class Meta:
        verbose_name = '资讯分类'
        verbose_name_plural = verbose_name
    def __str__(self):
        return self.category_content


class Tag(models.Model):
    tag_content =  models.CharField(max_length=20,verbose_name='热门标签')
    class Meta:
        verbose_name = '热门标签'
        verbose_name_plural = verbose_name
    def __str__(self):
        return self.tag_content


class Article(models.Model):
    poster = models.ImageField(upload_to='poster',default='imgs/default.png')
    title = models.CharField(max_length=100)
    source = models.CharField(max_length=100,blank=True,null=True)
    category = models.ForeignKey(Catetory,verbose_name='咨询分类')
    time = models.DateTimeField(auto_now_add=True)
    visit = models.IntegerField()
    article_content = models.TextField()
    pictures = models.ImageField(upload_to='pictures',default='/imgs/default.png')
    tag = models.ManyToManyField(Tag,verbose_name='所属标签')
    nav = models.ForeignKey(Nav)
    class Meta:
        verbose_name = '文章'
        verbose_name_plural = verbose_name
    def __str__(self):
        return self.title


class Hot(models.Model):
    hot_article = models.OneToOneField(Article,verbose_name='全站热门')
    class Meta:
        verbose_name = '全站热门'
        verbose_name_plural = verbose_name
    def __str__(self):
        return Article.title


class Community(models.Model):
    topic = models.CharField(max_length=100)
    update = models.IntegerField('更新时间')
    tiqzi_num = models.IntegerField('帖子数量')
    member_num = models.IntegerField('成员数')
    category = models.ManyToManyField(Catetory,verbose_name='帖子分类')
    tiezi = models.ForeignKey(Tiezi)
    class Meta:
        verbose_name = '圈子'
        verbose_name_plural = verbose_name
    def __str__(self):
        return self.topic


class Tiezi(models.Model):
    title = models.CharField(max_length=100)
    auth = models.ForeignKey(User,verbose_name='作者')
    visit = models.IntegerField('浏览量')
    response_num = models.IntegerField('回复量')
    response_user = models.ForeignKey(User,verbose_name='回复者')
    response_content = models.TextField('回复')
    class Meta:
        verbose_name = '帖子'
        verbose_name_plural = verbose_name
    def __str__(self):
        return self.title


class active(models.Model):
    community = models.ManyToManyField(Community)
    active_level = models.IntegerField('活跃度')
    def __str__(self):
        return Community.topic