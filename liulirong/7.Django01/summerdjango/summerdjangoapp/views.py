from django.shortcuts import render,HttpResponse

# 视图函数，处理http请求
# request：浏览器发送的数据
def master(request):
    html = "<h1>I am master</h1>"
    print(request)
    print(html)
    return HttpResponse(html)
def demo(request):
    # 默认从templates目录查找
    return render(request,'demo.html')
def login(request):
    return render(request,'login.html')
def registe(request):
    return render(request,'registe.html')