"""summerdjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from summerdjangoapp import views
from django.views.static import serve
from summerdjango import settings
# 在Django中所有的导入的根路径就是项目的根路径
urlpatterns = [
    # admin 后台的路由
    url(r'^admin/', admin.site.urls),
    url(r'^master$',views.master),
    # 路由名和视图函数名不一定要同名
    url(r'^demo',views.demo),
    url(r'^login',views.login),
    url(r'^registe',views.registe),
    url(r'^media1/(?P<path>.*)$',serve,{'document_root':settings.MEDIA_ROOT}),
    url(r'^check_username',views.check_username)
]