function check_username1() {
    document.getElementById('check1').innerHTML = '长度应在6~10之间<br>设置后不可更改';
}
function check_username2() {
    var x = document.getElementById('username').value;
    if (x.length < 6 || x.length > 10) {
        document.getElementById('check1').innerHTML = '<img src="/static/img/err_small.png">'
    }
    else {
        $(document).ready(
            function () {
                $.ajax({
//                本地访问：
                    url: '/check_username/',
                    dataType: 'json',
//                 跨域访问：
//                    url:'http://192.168.0.105/check_username/'
//                    dataType: 'jsonp',
//                    jsonp:'callback',
                    type: 'GET',
                    async: true,
                    data: {username: $('#username').val()},
                    beforeSend: function (xhr) {
                        console.log('发送前')
                    },
                    success: function (data) {
                        console.log(data,typeof data)
                        if (data == 0){
                            document.getElementById('check1').innerHTML = '<img src="/static/img/ok_small.png">'
                        }
                        else{
                            document.getElementById('check1').innerHTML ='你的用户名已被注册'
                        }
                    },
                    error: function (xhr, textStatus) {
                        console.log('错误');
                    },
                    complete: function () {
                        console.log('结束')
                    }

                })
            }
        )
    }
}
function check_tel1() {
    document.getElementById('check2').innerHTML = '请输入中国大陆手机号<br>其他用户不可见';
}
function check_tel2() {
    var x = document.getElementById('tel').value;
    if (x.length !== 11){
        document.getElementById('check2').innerHTML = '<img src="/static/img/err_small.png">'
    }
    else {
        document.getElementById('check2').innerHTML = '<img src="/static/img/ok_small.png">';
    }
}
function check_passwd1() {
    document.getElementById('check3').innerHTML = '密码为6-14个字符';
}
function check_passwd2() {
    var x = document.getElementById('passwd').value;
    if (x.length<6 || x.length>14){
        document.getElementById('check3').innerHTML = '<img src="/static/img/err_small.png">'
        return false;
    }
    else {
        document.getElementById('check3').innerHTML = '<img src="/static/img/ok_small.png">';
    }
}
function check_test1() {
    document.getElementById('check4').innerHTML = '请输入手机收到的验证码';
}
function check_test2() {
    var x = document.getElementById('test').value;
    if (x.length !== 4){
        document.getElementById('check4').innerHTML = '<img src="/static/img/err_small.png">'
        return false;
    }
    else {
        document.getElementById('check4').innerHTML = '<img src="/static/img/ok_small.png">';
    }
}