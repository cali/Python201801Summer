from django.db import models

# Create your models here.
# 一个类对应一个表
# a前提将app添加到setting中
class userinfo(models.Model):
    # 如果一个表没有主键，会自动生成名为id的主键
    email = models.EmailField()
    passwd = models.CharField(max_length=64)
class registelist(models.Model):
    username = models.CharField(max_length=10)
    tel = models.CharField(max_length=11)
    passwd = models.CharField(max_length=14)
    test = models.CharField(max_length=4)
class student(models.Model):
    id_test = models.AutoField(primary_key=True)  # 自增,主键
    username = models.CharField(max_length=48)  # Vachar类型，max_length是必填参数，在数据库层和Django表单验证中起作用# 默认的表单样式：TextInput
    content = models.TextField(max_length=48)  # 大文本类型,max_length体现在表单组件,在数据库层面不起作用,默认表单样式：Textarea
    alter_time = models.DateField(auto_now=True)  # 日期型，每次保存数据更新(insert/update)，最近修改时间
    alter_time2 = models.DateField()  # 日期型，每次保存数据更新，最近修改时间
    create_time = models.DateTimeField(auto_now_add=True)  # 日期时间型，每次新增数据时更新(insert)，操作日志，创建时间
    count = models.IntegerField()#默认表单样式输入工具：TextInput
    score = models.FloatField()
    money = models.DecimalField(max_digits=5, decimal_places=2)  # 精确浮点数，一共5位，2位小数位
    email = models.EmailField()  # 实际在数据库中是varchar  对输入数据增加了验证器，仅在表单验证时候生效
    myip = models.GenericIPAddressField(default=True)  # 最后登录的IP
    mybool = models.BooleanField(default=True)  # 可取True/False
    mybool2 = models.NullBooleanField()  # 可以为null/True/False
    avator = models.ImageField(upload_to='avator/', default='imgs/default.png', )  # default是照片来源，upload_to存放在该目录下
    # models.FileField  models.FilePathField，以上三个都有一个.url属性



