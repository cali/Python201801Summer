from django.contrib import admin

# Register your models here.
# 添加自己的Model到admin
from summerdjangoapp.models import userinfo,student,registelist
admin.site.register(userinfo)
admin.site.register(student)
admin.site.register(registelist)
